import { Component, OnInit, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { cardsInfoByID } from '../state/cardsInfo.status';
import { CardsInfoByIDStateModel } from '../state/cardsInfo.model';
import { GetCardsInfoByIDAction } from '../state/cardsInfo.action';
import { SharedDataService } from '../shared-data.service';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss'],
  animations: [
    trigger('cardAnimation', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition(':enter', [style({ opacity: 0, transform: 'translateX(-100%)' }),
        animate(1000),
      ])
    ])
  ]
})
export class UserInfoComponent implements OnInit, OnDestroy {
  @Select(cardsInfoByID) getCardsInfoByID$ !: Observable<CardsInfoByIDStateModel>
  getCardDataByID !:any;
  getCardDataSubscription : Subscription = new Subscription()
  constructor(
    private store: Store,
    private shared: SharedDataService,
    private route: ActivatedRoute,
    private ngxLoader: NgxUiLoaderService,
    ) { 
     
      this.shared.id = route.snapshot.params['id']
    }

  ngOnInit(){
    this.store.dispatch(new GetCardsInfoByIDAction());
    this.getCardDataSubscription = this.getCardsInfoByID$.subscribe((res:any)=>{
      this.showLoader();
      this.getCardDataByID = res.data.data      
    })
    this.hideLoader();
  }
  showLoader() {
    this.ngxLoader.start();
  }
  hideLoader() {
    this.ngxLoader.stop();
  }
  ngOnDestroy() {
    this.getCardDataSubscription.unsubscribe();
  }
}
