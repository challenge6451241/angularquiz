import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable ,Subject,Subscription, catchError} from 'rxjs';
import { cardsInfo } from '../state/cardsInfo.status';
import { CardsInfoStateModel } from '../state/cardsInfo.model';
import { GetCardsInfoAction } from '../state/cardsInfo.action';
import { SharedDataService } from '../shared-data.service';
import { Router } from '@angular/router';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { cardsInfoByID } from '../state/cardsInfo.status';
import { CardsInfoByIDStateModel } from '../state/cardsInfo.model';
import { GetCardsInfoByIDAction } from '../state/cardsInfo.action';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit, OnDestroy {
  private searchInputSubject = new Subject<string>();
  searchTerm: string = '';
  load=false;
  search=false;
  noData= false;
  filterValue='';
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @Select(cardsInfo) getCards$ !: Observable<CardsInfoStateModel>
  getCards!: any;
  total!:any
  value!:'';
  getCardsSubscription: Subscription = new Subscription();
  @Select(cardsInfoByID) getCardsInfoByID$ !: Observable<CardsInfoByIDStateModel>
  getCardDataByID !:any;
  getCardDataSubscription : Subscription = new Subscription()
  constructor(
    private store: Store,
    private shared : SharedDataService,
    private router: Router,
    private ngxLoader: NgxUiLoaderService,
    ) { 
      this.shared.size = '6';
      this.shared.page = '1';
      this.setupSearch();
    }
  ngOnInit(): void {
    search : new FormControl('');
    this.store.dispatch( new GetCardsInfoAction());
    this.getCardsSubscription = this.getCards$.subscribe((res:any)=>{
      this.showLoader();
      this.total = res.data.total
      this.getCards= res.data.data;            
    })
    this.hideLoader();
    this.getCardsData();
  }
  getCardsData(){
    return this.getCards;
  }
  onClick(element: any){
      this.router.navigate(['usersCard',element])
  }
  onPaginationChange(event: PageEvent){
    let page  = this.paginator.pageIndex+1;
    let size = this.paginator.pageSize
    this.shared.page = page.toString();
    this.shared.size = size.toString();
    this.store.dispatch( new GetCardsInfoAction());
    this.getCards$.subscribe((res:any)=>{
      this.getCards= res.data.data; 
      this.hideLoader();

    })
    this.getCardsData();
  }
  private setupSearch() {
    this.searchInputSubject
      .pipe(
        debounceTime(300), 
        distinctUntilChanged() 
      )
      .subscribe(() => {
        this.showLoader();
        this.shared.id = this.searchTerm;
        this.store.dispatch(new GetCardsInfoByIDAction());
        this.getCardDataSubscription = this.getCardsInfoByID$ .pipe(
      catchError((error: any) => {
        return error
      }))
    .subscribe((res:any)=>{
      this.getCardDataByID = res.data.data      
    })
    if(this.getCardDataByID != null || this.getCardDataByID !=''){
      this.search = true;
    }else{
      this.search = false;
    }
    this.hideLoader();
      });
  }

  applyFilter(event: Event){
    this.searchTerm = (event.target as HTMLInputElement).value;
    this.searchInputSubject.next(this.searchTerm);
    this.shared.id = this.searchTerm;
    this.hideLoader();
  }
  showLoader() {
    this.ngxLoader.start();
  }
  hideLoader() {
    this.ngxLoader.stop();
  }
  ngOnDestroy() {
    this.getCardsSubscription.unsubscribe();
    this.getCardDataSubscription.unsubscribe();
  }
}
