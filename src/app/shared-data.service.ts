import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {
  id : string ='';
  size : string  = '';
  page : string  = '';
  constructor() { }
}
