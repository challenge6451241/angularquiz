import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HttpServiceService  {

  constructor(private http: HttpClient) { }
  getCardsData(page: string){
    return this.http.get<any>("https://reqres.in/api/users?page="+page);
  }
  getCardInfoById(id: string){
    return this.http.get<any>('https://reqres.in/api/users/'+id)
  }
}
