import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersListComponent } from './users-list/users-list.component';
import { UserInfoComponent } from './user-info/user-info.component';


const routes: Routes = [
  {path: '', component:UsersListComponent },
  {path:'cards', component: UsersListComponent},
  {path : 'usersCard/:id', component:UserInfoComponent },
  {path: '**', redirectTo: "cards"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
