export class GetCardsInfoAction {
    static readonly type = '[CardsInfo] Get Info'
}
export class GetCardsInfoByIDAction {
    static readonly type = '[CardsInfoById] Get Info By ID'
}

