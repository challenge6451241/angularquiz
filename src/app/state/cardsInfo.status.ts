import { Injectable } from "@angular/core";
import { Action, Selector, State, StateContext, Store } from "@ngxs/store";
import { tap } from "rxjs";
import { HttpServiceService } from "../http-service.service";
import { GetCardsInfoAction, GetCardsInfoByIDAction } from "./cardsInfo.action";
import { CardsInfoStateModel, CardsInfoByIDStateModel } from "./cardsInfo.model";
import { SharedDataService } from "../shared-data.service";
@State<CardsInfoStateModel>({
    name:'cardsInfo',
    defaults:{
        data:[],
    }
})
@Injectable()
export class cardsInfo{
    constructor(
        private cardsData :HttpServiceService,
        private store:Store,
        private shared:SharedDataService){}
    @Selector()
    static getCardsInfoSelector(state:CardsInfoStateModel){
        return state
    }
    @Action(GetCardsInfoAction)
    getCardsInfoAction({getState,setState}:StateContext<CardsInfoStateModel>){
       const page = this.shared.page
        return this.cardsData.getCardsData(page).pipe(
            tap((res)=>{
                setState({
                    ...getState(),
                    data:res
                })
            })
        )
    }

}

//Get info by id
@State<CardsInfoByIDStateModel >({
    name:'cardsInfoByID',
    defaults:{
        data:[],
    }
})
@Injectable()
export class cardsInfoByID{
    constructor(
        private cardsData :HttpServiceService,
        private store:Store,
        private shared: SharedDataService){}
    @Selector()
    static getCardsInfoByIDSelector(state:CardsInfoByIDStateModel ){
        return state.data
    }
    @Action(GetCardsInfoByIDAction)
    getCardsInfoByIDAction({getState,setState}:StateContext<CardsInfoByIDStateModel >){
       const id = this.shared.id
        return this.cardsData.getCardInfoById(id).pipe(
            tap((res)=>{
                setState({
                    ...getState(),
                    data:res
                })
            })
        )
    }

}

